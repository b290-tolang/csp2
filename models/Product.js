const mongoose = require('mongoose') 

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Product name is required']
    },
    description: {
        type: String,
        required: [true, 'Product description is required']
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Product price is required.']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    soldQuantity: {
        type: Number,
        min: 0,
        default: 0
    },
    stockQuantity: {
        type: Number,
        min: 0,
        required: [true, 'Product stock quantity required']
    },
    images: [
        {
            type: String,
        }
    ],
    averageRating: {
        type: Number,
        min: 0
    },
    category:{
        type: String,
        required: true
    }
})

const Product = mongoose.model('Product', productSchema)
module.exports = Product