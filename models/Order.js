const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'UserId is required']
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, 'Product id is required']
            },
            price: {
                type: Number,
                min: 0,
                required: [true, 'Product price quantity is required']
            },
            quantity: {
                type: Number,
                min: 0,
                required: [true, 'Product order quantity is required']
            },
            subtotalAmount: {
                type: Number
            }
        }
    ],
    totalAmount: {
        type: Number
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    orderStatus: {
        type: String,
        default: 'In Progress',
        enum: ['In Progress', 'Completed', 'Cancelled']
    }
})

const Order = mongoose.model('Order', orderSchema)
module.exports = Order