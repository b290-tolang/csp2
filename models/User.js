const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required.']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    cartItems: [
        new mongoose.Schema({
            productId: {
                type: String,
                required: [true, 'Product id is required']
            },
            price: {
                type: Number,
                min: 0,
                required: [true, 'Product price is required']
            },
            quantity: {
                type: Number,
                required: [true, 'Product order quantity is required']
            },
            subtotalAmount: {
                type: Number
            }
        },
        {
            methods: {
                recalculateSubtotalAmount() {
                    this.subtotalAmount = this.price * this.quantity
                },
                setPrice(newPrice) {
                    this.price = newPrice
                }
            }
        })
    ],
    totalAmountPayable: {
        type: Number,
        min: 0
    }

},
{
    methods: {
        recalculateTotalAmount() {
            this.totalAmountPayable = this.cartItems.map(item => item.subtotalAmount).reduce((sum, num) => sum + num, 0)
        }
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;