const mongoose = require('mongoose')

const reviewSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: [true, 'OrderId is required']
    },
    userId: {
        type: String,
        required: [true, 'UserId is required']
    },
    productId: {
        type: String,
        required: [true, 'ProductId is required']
    },
    comment: {
        type: String
    },
    rating: {
        type: Number,
        required: [true, 'Rating is required'],
        enum:[1,2,3,4,5]
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

const Review = mongoose.model('Review', reviewSchema)
module.exports = Review