const Product = require('../models/Product')
const User = require('../models/User')
const APIError = require('../utils/APIError')

/* 
    {
        name
        description
        price
    }
*/
// creates products
// logs a message in the console if duplicate products are detected.
module.exports.createProduct = async (reqBody) => {
    const newProduct = new Product(reqBody)

    // log in console for any duplicate products
    Product.countDocuments({name: newProduct.name})
        .then((duplicateCount) => {
            if(duplicateCount){
                console.log(`${duplicateCount} duplicate product${duplicateCount>1?'s':''} found for ${newProduct.name}`)
            }
        })
        .catch((err) => console.log(err))

    // if(duplicateCount){console.log(`${duplicateCount} duplicate product${duplicateCount>1?'s':''} found for ${newProduct.name}`)}
    
    const saveResult = await newProduct.save()
                                       .catch(err => {
                                            throw new APIError(400, `Error on Product: ${newProduct.name}\n${err}`)
                                       })
    return (saveResult) ? true : false
}

module.exports.createMultipleProducts = async (reqBody) => {
    const result = await Product.insertMany(reqBody).catch(err => {throw new APIError(400, err)})
    return result ? true : false
}

module.exports.getAllProducts = async () => {
    return await Product.find({})
}

module.exports.getAllActiveProducts = async () => {
    return await Product.find({isActive: true})
}

module.exports.getOneProduct = async (productId) => {
    
    const product = await Product.findById(productId).catch(err => {throw new APIError(404, err)})
    if(product == null){
        throw new APIError(404, "Product doesn't exist")
    }
    return product

}

module.exports.updateProduct = async (productId, reqBody) => {

    // This line will throw an error if any update is invalid, e.g. price is less than zero
    console.log('update reqBody', reqBody)
    let updatedProduct = await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new: true})
    .catch(err => {throw new APIError(404, err)})

    // recalculate subtotal on all of carts with updated product
    const usersWithProduct = await User.find({"cartItems.productId": {$eq: productId}})

    // Create an array of Promises that updates the Users which have the updated product in their shoppingCart
    let promiseArray = usersWithProduct.map(user => {
        let itemToUpdate = user.cartItems.find((item) => item.productId == productId)
        itemToUpdate.setPrice(updatedProduct.price)
        itemToUpdate.recalculateSubtotalAmount()
        user.recalculateTotalAmount()
        return user.save()
    })

    // If any error occurs during saving then we throw error
    await Promise.all(promiseArray).catch(err => {throw new APIError(400, err)})

    return (updatedProduct) ? true : false
}

module.exports.archiveProduct = async (productId) => {
    let archivedProduct = await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new: true})
    return (archivedProduct) ? true : false
}

module.exports.activateProduct = async (productId) => {
    let activatedProduct = await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new: true})
    return (activatedProduct) ? true : false
}

module.exports.searchProducts = async (searchFor) => {
    const regexPattern = searchFor.replace(/(\w)/g, "$1.*")
    return await Product.aggregate([
        {
            $match: {
                $or: [
                    {name: {$regex: regexPattern, $options: 'i'}},
                    {description: {$regex: regexPattern, $options: 'i'}}
                ]
            }
        }
    ]) 
}

module.exports.checkDuplicateProducts = async (reqBody) => {

}