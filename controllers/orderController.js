const Order = require('../models/Order')
const Product = require('../models/Product')
const APIError = require('../utils/APIError')

module.exports.createOrder = async (orderBody) => {

    // Creates an array of Promises that checks if each Product in the order has sufficient stock quantity
    let promiseArray = orderBody.products.map( itemOrder => {
        return Product.findById(itemOrder.productId)
            .then(product => {
                if(itemOrder.quantity > product.stockQuantity){
                    return `Error: Order quantity (${itemOrder.quantity}) exceeds Product stock (${product.stockQuantity}) for ${product.name}`
                }
            })
            .catch(err => {throw new APIError(400, err)}) //If an error occurs, this will pass the error to the error handler in orderRoute
                })
    
    // waits for all promises inside promiseArray to resolve 
    // and then filter out array elements that have undefined value
    await Promise.all(promiseArray)
        .then(resultArray => resultArray.filter(result => result))
        .then(filteredResult => {
            if(filteredResult.length > 0){throw new APIError(403, filteredResult.toString())}
        })
    
    // Update Product stockQuantity and soldQuantity
    for(let itemOrder of orderBody.products) {
        Product.findById(itemOrder.productId)
            .then(product => {
                product.stockQuantity -= itemOrder.quantity
                product.soldQuantity += itemOrder.quantity
                product.save()
            })
    }
    
    // Create the Order
    const newOrder = new Order(orderBody)
    const saveResult = await newOrder.save()
    return (saveResult) ? true : false
}

module.exports.getUserOrders = async (userId) => {
    const foundOrders = await Order.find({userId}).sort({purchasedOn: 'asc'})
    return foundOrders
}

module.exports.getAllOrders = async () => {
    const allOrders = await Order.find({}).sort({purchasedOn: 'asc'})
    return allOrders
}

/* 
    {
        orderId:
    }
*/
/* 
    1. retrieve an order by using findOne, and querying for userId and reqBod.orderId
    2. the result will be null if the user doesn't have an order with orderId, or if the order doesn't exists
    3. If the order exists, set orderStatus to Cancelled
    4. save the order and return true if save is successful, false otherwise.
*/
module.exports.cancelOrder = async (userId, reqBody) => {
    const userOrder = await Order.findOne({userId, _id: reqBody.orderId})

    if(userOrder == null) {
        throw new APIError(404, "User doesn't have this order")
    }
    
    let result = await setOrderStatus(userOrder, 'Cancelled')
    return (result) ? true : false
}

module.exports.completeOrder = async (reqBody) => {
    const order = await Order.findOne({ _id: reqBody.orderId})

    if(order == null) {
        throw new Error("Order doesn't exist")
    }

    let result = await setOrderStatus(order, 'Completed')
    return (result) ? true : false
}

async function setOrderStatus (order, newStatus) {
    order.orderStatus = newStatus
    return await order.save()
}