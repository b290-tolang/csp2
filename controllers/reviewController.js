const Review = require('../models/Review')
const Order = require('../models/Order')
const Product = require('../models/Product')
const productController = require('./productController')
const APIError = require('../utils/APIError')
/* 
reqBody = {orderId, productId, comment, rating}
/* 
    1.Get order using findOne with arguments orderId & userId, and check if order.products includes productId
    2. if not valid, log a message and return false
    3. If valid, create a new Review document named newReview
    4. save newReview, update and return true if save is successful, false otherwise.
*/
module.exports.createReview = async (userId, reqBody) => {

    const order = await Order.findOne({_id: reqBody.orderId, userId})
    if(order == null) {
        throw new APIError(404, "User doesn't have this order")
    }

    const index = order.products.findIndex(item => item.productId == reqBody.productId)

    if(index == -1){
        throw new APIError(404, "Product is not in this order")
    }

    // check for duplicate reviews. Counts reviews with the same orderId & productId.
    if((await Review.countDocuments({orderId: reqBody.orderId, productId: reqBody.productId})) > 0){
        throw new APIError(409, "A review already exists for this Order & Product")
    }

    // create review
    let newReview = new Review(
        {
            orderId: reqBody.orderId,
            userId: userId,
            productId: reqBody.productId,
            comment: reqBody.comment,
            rating: reqBody.rating
        }
    )
    
    const saveResult = await newReview.save()
    await this.updateProductAverageRating(reqBody.productId)
    return (saveResult) ? true : false;
}

module.exports.getProductReviews = async (productId) => {
    // check if product exists
    
    if((await Product.findById(productId)) == null) {
        throw new APIError(404, 'Product does not exist')
    }

    const reviews = Review.find({productId}, {orderId: 0, userId: 0})
    return reviews
}

module.exports.updateProductAverageRating = async (productId) => {
    // aggregate to calculate average rating
    const result = await Review.aggregate([
        {$match: {productId: productId}},
        {$group: {_id: null, avgRating: {$avg: "$rating"}}}
    ])

    const updateResult = await productController.updateProduct(productId, {averageRating: result[0].avgRating})
    return updateResult? true : false
}

module.exports.getOneProductReview = async (reviewId) => {

    const review = await Review.findById(reviewId, {orderId: 0, userId: 0})
    if(review == null) {
        throw new Error("Review doesn't exist")
    }
    return review
}


/* 
    comment:
    rating:
    reviewId:
    userId:
*/
/* 
    1. use findOneAndUpdate to find a review that has its reviewId as _id, and the same userId
    2. update using the comment with reqBody using $set
    3 return true if updatedReview is truthy
*/
module.exports.userEditReview = async (userId, reviewId, reqBody) => {
    const updatedReview = await Review.findOneAndUpdate({userId, _id: reviewId}, {$set: reqBody},{new: true})
    
    // update Product average rating
    if(updatedReview) {this.updateProductAverageRating(updatedReview.productId)}

    return updatedReview ? true : false
}

module.exports.userDeleteReview = async (userId, reviewId) => {
    const deletedReview = await Review.findOneAndDelete({userId, _id: reviewId})

    // update Product average rating
    if(deletedReview) {this.updateProductAverageRating(deletedReview.productId)}

    return deletedReview ? true : false
}

module.exports.adminDeleteReview = async (reviewId) => {
    const deletedReview = await Review.findByIdAndDelete({_id: reviewId})

    // update Product average rating
    if(deletedReview) {this.updateProductAverageRating(deletedReview.productId)}

    return deletedReview? true : false
}