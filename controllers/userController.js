const bcrypt = require("bcrypt");
const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const APIError = require("../utils/APIError");

const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

module.exports.registerUser = async (userData) => {
  // checks if email is already taken
  if (await this.checkEmailExists(userData.email)) {
    throw new APIError(409, "Email already exists");
  }

  const { firstName, lastName, email } = userData;
  const newUser = new User({ firstName, lastName, email });
  newUser.password = bcrypt.hashSync(userData.password, 12);
  const saveResult = await newUser.save().catch((err) => {
    throw new APIError(401, err);
  });
  return saveResult ? true : false;
};

module.exports.loginUser = async (reqBody) => {
  const foundUser = await User.findOne({ email: reqBody.email });

  if (foundUser == null) {
    throw new APIError(404, "User does not exist");
  }

  const isPasswordCorrect = bcrypt.compareSync(
    reqBody.password,
    foundUser.password
  );

  if (isPasswordCorrect) {
    return { access: auth.createAccessToken(foundUser) };
  } else {
    throw new APIError(401, "Password is incorrect.");
  }
};

module.exports.checkEmailExists = async (email) => {
  const result = await User.find({ email });
  return result.length > 0;
};

module.exports.getUserPofile = async (userId) => {
  const userProfile = await User.findById(userId);
  if (userProfile == null) {
    throw new APIError(404, "User does not exist");
  }
  userProfile.password = "";
  return userProfile;
};

module.exports.getAllUserPofile = async () => {
  const users = await User.find({});
  users.forEach((user) => {
    user.password = "";
  });
  return users;
};

// Valid request body
/* {
    productId: 
    quantity: 
} */
/* 
    1. Find the user document to be added with an item, and the product document that will be added
    2. Check if product exists, or if product is active. Proceed if both are true
    3. Check if the product already exists in the cart arrray and what its index is.
    4. If the product is already in the array, add reqBody.quantity to existing product quantity
    5. If product is not in the array, push a new document to the cart based on reqBody's contents
    6. use user document's recalculateTotalAmount method to recalculate user's totalAmount field
    7. Save user, and return true on successful save, and false otherwise.
*/
module.exports.addItemToCart = async (userId, reqBody) => {
  // retrieve user and check if user exists
  const user = await User.findById(userId);
  if (user == null) {
    throw new APIError(404, "User doesn't exist.");
  }

  // retrieve product and check if product exists
  const product = await productController.getOneProduct(reqBody.productId);
  if (product == null) {
    throw new APIError(404, "Product does not exist");
  }

  //
  if (!product.isActive) {
    throw new APIError(403, "Product is not active");
  }

  // check if productId is in the cart already
  const productIndex = user.cartItems.findIndex(
    (item) => item.productId == product._id
  );

  //productId is already in the cart
  if (productIndex !== -1) {
    //add quantity to existing item in the cart
    // add a plus symbol before the quantity to parse it.
    user.cartItems[productIndex].quantity += +reqBody.quantity;
    user.cartItems[productIndex].recalculateSubtotalAmount();
  } else {
    // push a new subdocument to cartItems
    user.cartItems.push({
      productId: reqBody.productId,
      quantity: reqBody.quantity,
      price: product.price,
      subtotalAmount: product.price * reqBody.quantity,
    });
  }

  // Call User model method to recalculate total amount
  user.recalculateTotalAmount();

  const saveResult = await user.save();

  return saveResult ? true : false;
};

// Cart Checkout
/* 
    1. Find the user using User.findById
    2. Use orderController's createOrder method to 
    create an order and pass userId & user.cart
    3. For totalAmount, use array map & reduce methods to sum up each product's
    subtotalAmount 
    4. If order is successfully made, createOrder will return true. We save this
    value in the orderSuccess variable.
    5. If orderSuccess is true, we use the mongoose document method .set to set
    the cart to an empty array. user's totalAmountPayable field is also set to zero.
    6. We use orderSuccess as the return value for our method
*/
module.exports.checkoutCart = async (userId) => {
  const user = await User.findById(userId);

  if (user.cartItems.length == 0) {
    throw new APIError(400, "User Cart is empty");
  }

  const orderSuccess = await orderController.createOrder({
    userId,
    products: [...user.cartItems], //pass a clone of user cartItems
    totalAmount: user.cartItems
      .map((item) => item.subtotalAmount)
      .reduce((sum, num) => sum + num, 0),
  });

  // if order is successfully created, clear cart
  if (orderSuccess) {
    // set cart to an empty array
    user.set("cartItems", []);
    user.totalAmountPayable = 0;
    console.log("user after checkout", user);
    await user.save();
  }

  return orderSuccess;
};

module.exports.grantAdminAcess = async (email) => {
  const user = await User.findOne({ email });

  if (user == null) {
    throw new APIError(404, "User doesn't exist");
  }

  if (user.isAdmin) {
    throw new APIError(403, "User is already an ADMIN");
  }

  // grant admin access to user
  user.isAdmin = true;
  let updatedUser = await user.save();
  return updatedUser ? true : false;
};

module.exports.toggleAdminAcess = async (email) => {
  const user = await User.findOne({ email });

  if (user == null) {
    throw new APIError(404, "User doesn't exist");
  }

  // grant admin access to user
  user.isAdmin = !user.isAdmin;
  let updatedUser = await user.save();
  return updatedUser ? true : false;
};

/* 
    1. Find the user & the product documents using findById
    2. Search for the product inside the user's cart
    3. Return false if product does not exist 
    3. If the product is not in the cart, return false
    4. If the product is in the cart, update it's quantity and subtotalAmount
    5. Save the user
*/
module.exports.changeCartQuantities = async (userId, reqBody) => {
  const user = await User.findById(userId);
  const product = await Product.findById(reqBody.productId);

  if (product == null) {
    throw new APIError(404, "Product does not exist");
  }

  const itemIndex = user.cartItems.findIndex(
    (item) => item.productId == reqBody.productId
  );

  if (itemIndex == -1) {
    throw new APIError(404, "Product is not in the cart");
  }

  user.cartItems[itemIndex].quantity = reqBody.quantity;
  user.cartItems[itemIndex].recalculateSubtotalAmount();
  user.recalculateTotalAmount();

  const saveResult = await user.save();
  return saveResult ? true : false;
};

/* 
    1. Find the user, and check if the productId in the reqBody exists in its cart
    2. If the product doesn't exist, return false and log a message in the console
    3. If the product exists in the cart, use splice to remove the product
    4. save user and return true for successful save, false otherwise

*/
module.exports.removeProducts = async (userId, reqBody) => {
  const user = await User.findById(userId);
  const itemIndex = user.cartItems.findIndex(
    (item) => item.productId == reqBody.productId
  );

  if (itemIndex == -1) {
    throw new APIError(404, "Product is not in the cart");
  }

  user.cartItems.splice(itemIndex, 1);
  user.recalculateTotalAmount();
  const saveResult = await user.save();
  return saveResult ? true : false;
};

module.exports.getCart = async (userId) => {
  const user = await User.findById(userId);
  if (user == null) {
    throw new APIError(404, "User doesn't exist.");
  }
  return user.cartItems;
};

module.exports.changePassword = async (userId, reqBody) => {
  const user = await User.findById(userId);
  if (!bcrypt.compareSync(reqBody.oldPassword, user.password)) {
    throw new APIError(401, "Incorrect Password");
  }
  user.password = bcrypt.hashSync(reqBody.newPassword, 12);
  let updatedUser = await user.save();
  return updatedUser ? true : false;
};
