const express = require('express')
const router = express.Router()

const productController = require('../controllers/productController')
const reviewController = require('../controllers/reviewController')

const auth = require('../auth')
const catchAsync = require('../utils/catchAsync')
const APIError = require('../utils/APIError')
// const { route } = require('./userRoute') ??? 

// create Product, ADMIN
router.post('/', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

        let result = await productController.createProduct(req.body)
        res.status(201).send(result)
}))

// create Multiple Products, ADMIN
// all products must be valid for the request to be completed
router.post('/create-multiple', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

    let result = await productController.createMultipleProducts(req.body)
    res.status(201).send(result)

}))

// retrieve all active products only
router.get('/', catchAsync(async(req, res) => {

        let result = await productController.getAllActiveProducts()
        res.send(result)

}))

// retrieve all products, ADMIN
router.get('/all', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

        let result = await productController.getAllProducts()
        res.send(result)

}))


// 'products/search?searchFor='gtx3060'
// searches product name or description for the keyword
router.get('/search', catchAsync(async (req, res) => {

    let results = await productController.searchProducts(req.query.searchFor)
    res.send(results)

}))


// retrieve one specific product
// if the product is inactive, only admin can retrieve the product
router.get('/:productId', catchAsync(async (req, res) => {

        const userData = auth.decode(req.headers.authorization)
        let result = await productController.getOneProduct(req.params.productId)

        // if there is a result, and the is not active and user is not authenticated or user is not an admin
        if(result && !result.isActive && (!userData || !userData.isAdmin)){
                throw new APIError('User cannot access inactive product')
        }

        res.send(result)

}))

// update one product, ADMIN
router.put('/:productId', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

        let result = await productController.updateProduct(req.params.productId, req.body)
        res.send(result) 

}))

// archive one product, ADMIN
router.put('/:productId/archive', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

        let result = await productController.archiveProduct(req.params.productId)
        res.send(result)

}))

// activate one product, ADMIN
router.put('/:productId/activate', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

        let result = await productController.activateProduct(req.params.productId)
        res.send(result)

}))

// get all product reviews for a product
router.get('/:productId/reviews', catchAsync(async (req, res) => {
    let result =  await reviewController.getProductReviews(req.params.productId)
    res.send(result)
}))


router.use((err, req, res, next) => {
    console.log('productRoute Error handler')
    const {message, statusCode = 500} = err
    console.log(message)
    res.status(statusCode).send(false)
})
module.exports = router