const express = require("express");
const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/userController");
const orderController = require("../controllers/orderController");
const catchAsync = require("../utils/catchAsync");
const APIError = require("../utils/APIError");

// checkouts logged in user's cart
router.post(
  "/user/cart/checkout",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    // throw Error if isAdmin is true
    if (userData.isAdmin) {
      throw new APIError(403, "Order Creation is for NON-Admin only\n");
    }

    const result = await userController.checkoutCart(userData.id);
    res.status(201).send(result);
  })
);

// Retrieves logged in user's details
router.get(
  "/user/profile",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.getUserPofile(userData.id);
    res.send(result);
  })
);

router.post(
  "/admin/getProfile",
  auth.verifyUserIsAdmin,
  catchAsync(async (req, res) => {
    const result = await userController.getUserPofile(req.body.userId);
    res.send(result);
  })
);

router.get(
  "/admin/getAllProfiles",
  auth.verifyUserIsAdmin,
  catchAsync(async (req, res) => {
    const result = await userController.getAllUserPofile();
    res.send(result);
  })
);

/* 
{
    oldPassword, 
    newPassword
} 
*/
// change user password. requires old password and new password in the request body.
router.put(
  "/user/reset-password",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.changePassword(userData.id, req.body);
    res.send(result);
  })
);

// Retrieves' logged in user's orders in ascending order(Newest orders first)
router.get(
  "/user/orders",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await orderController.getUserOrders(userData.id);
    res.send(result);
  })
);

// Grant admin access to a user. Requires admin authorization.
// requires email
router.put(
  "/admin/grant-admin-access",
  auth.verifyUserIsAdmin,
  catchAsync(async (req, res) => {
    const result = await userController.grantAdminAcess(req.body.email);
    res.send(result);
  })
);

router.put(
  "/admin/toggle-admin-access",
  auth.verifyUserIsAdmin,
  catchAsync(async (req, res) => {
    const result = await userController.toggleAdminAcess(req.body.email);
    res.send(result);
  })
);

// Register a user. Returns false if the email in the request is already in use in the database.
router.post(
  "/register",
  catchAsync(async (req, res) => {
    const result = await userController.registerUser(req.body);
    res.status(201).send(result);
  })
);

router.post(
  "/login",
  catchAsync(async (req, res) => {
    const result = await userController.loginUser(req.body);
    res.send(result);
  })
);

// used for checking if an email is already used in our database
// use the request body to set the email being checked
router.post(
  "/checkEmail",
  catchAsync(async (req, res) => {
    const result = await userController.checkEmailExists(req.body.email);
    res.send(result);
  })
);

/* 
{
    productId: 
    quantity: 
}
*/
// user adds product to cart
// product details are in the request body
router.post(
  "/user/cart",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.addItemToCart(userData.id, req.body);
    res.status(201).send(result);
  })
);

// retrieve's authenticated user's cart contents
router.get(
  "/user/cart",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.getCart(userData.id);
    res.send(result);
  })
);

/* 
{
    productId: 
    quantity: 
}
*/
// change quantities of items in the cart
router.put(
  "/user/cart/change-quantities",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.changeCartQuantities(
      userData.id,
      req.body
    );
    res.send(result);
  })
);

/* 
{
    productId: 
}
*/
// remove a single product from the user cart
router.put(
  "/user/cart/remove-product",
  auth.verify,
  catchAsync(async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const result = await userController.removeProducts(userData.id, req.body);
    res.send(result);
  })
);

router.use((err, req, res, next) => {
  const { message, statusCode = 500 } = err;
  console.log(message);
  res.status(statusCode).send(false);
});

module.exports = router;
