const express = require('express')
const router = express.Router()

const orderController = require('../controllers/orderController')
const auth = require('../auth')
const catchAsync = require('../utils/catchAsync')

// get all orders, ADMIN
router.get('/admin/all-orders', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

    const result = await orderController.getAllOrders()
    res.send(result)
}))

// router.get('/user/orders')


router.post('/user/cancel-order', auth.verify, catchAsync(async (req, res) => {
    
    const userData = auth.decode(req.headers.authorization)
    const result = await orderController.cancelOrder(userData.id, req.body)
    res.send(result)
}))

// set orderStatus to Completed, ADMIN
router.post('/admin/complete-order', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {
    
    const result = await orderController.completeOrder(req.body)
    res.send(result)
}))

// Create an order
router.post('/', auth.verifyUserIsAdmin, catchAsync(async (req, res) => {

    const result = await orderController.createOrder(req.body)
    res.status(201).send(result)
}))

// order route custom error handler
router.use((err, req, res, next) => {
    console.log('Order route custom error handler')
    const {message, statusCode = 500} = err
    console.log(message)
    res.status(statusCode).send(false)
})

module.exports = router