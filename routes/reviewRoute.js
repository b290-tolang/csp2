const express = require('express')
const router = express.Router()
const reviewController = require('../controllers/reviewController')

const auth = require('../auth')
const catchAsync = require('../utils/catchAsync')


/* {
    orderId
    productId
    comment
    rating
} */
// create a review
router.post('/', auth.verify, catchAsync(async (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    const result = await reviewController.createReview(userData.id, req.body)
    res.status(201).send(result)
}))


// get a specific review
router.get('/:reviewId', catchAsync(async (req, res) => {
    const result = await reviewController.getOneProductReview(req.params.reviewId)
    res.send(result)
}))

/* 
{
        comment:
        rating:
    }
    */
// edit a specific review if user is the owner,
router.put('/:reviewId', auth.verify, catchAsync(async (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    const result = await reviewController.userEditReview(userData.id, req.params.reviewId, req.body)
    res.send(result)
    
}))

// delete a review if user is the owner, then updates the product's average rating
router.delete('/:reviewId', auth.verify, catchAsync(async (req, res) => {
    
    const userData = auth.decode(req.headers.authorization)
    const result = await reviewController.userDeleteReview(userData.id, req.params.reviewId)
    res.send(result)
}))

// admin deletes any review
router.delete('/admin/:reviewId', auth.verifyUserIsAdmin, catchAsync( async (req, res) => {
    
    const result = await reviewController.adminDeleteReview(req.params.reviewId)
    res.send(result)
}))

router.use((err, req, res, next) => {
    console.log('Review route custom error handler')
    const {message, statusCode = 500} = err
    console.log(message)
    res.statusCode(statusCode).send(false)
})



module.exports = router