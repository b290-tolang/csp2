const jwt = require('jsonwebtoken')

const secret = process.env.SECRET || 'ZUITTCSP2SECRET'

module.exports.createAccessToken = (user) => {
    return jwt.sign({
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }, secret, {})
}

// middleware for nonadmin verification
module.exports.verify = (req, res, next) => {
    let token  = req.headers.authorization

    if(typeof token == "undefined"){
        return res.status(401).send({auth: "failed"})
    }

    token = token.slice(7)
    return jwt.verify(token, secret, (err, payload) => {
        if(err) {
            return res.status(401).send({auth: "failed"})
        }
        return next()
    })

}

// middleware for admin verification
module.exports.verifyUserIsAdmin = (req, res, next) => {
    let token  = req.headers.authorization

    if(typeof token == "undefined"){
        return res.status(400).send({auth: "failed"})
    }

    token = token.slice(7)
    return jwt.verify(token, secret, (err, payload) => {
        if(err) {
            return res.status(401).send({auth: "failed"})
        }

        if(!payload.isAdmin){
            return res.status(401).send({auth: "Admin authorization required"})
        }

        return next()
    })
}

module.exports.decode = (token) => {
    if(typeof token === 'undefined'){
        return null
    }

    token = token.slice(7)
    return jwt.verify(token, secret, (err, payload) => {

        if (err) {
            return null
        }

        return payload
    })
}