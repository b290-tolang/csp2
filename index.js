const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoute = require('./routes/userRoute')
const productRoute = require('./routes/productRoute')
const orderRoute = require('./routes/orderRoute')
const reviewRoute = require('./routes/reviewRoute')

const app = express()

const port = 4000;

// Mongoose connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.jhyg7zw.mongodb.net/e-commerce?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });


let db = mongoose.connection
db.on("error", console.error.bind(console, "DB Connection error"))
db.once("open", () => console.log('We\'re connected to MongoDB Atlas'));


// Middlewares
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended : true}));


// app.get('/', (req, res) => {
//     res.send('Hello world!')
// })

app.get('/', (req, res) => {
    res.send('Welcome to My E-Commerce API!')
})

app.use('/users', userRoute)
app.use('/products', productRoute)
app.use('/orders', orderRoute)
app.use('/reviews', reviewRoute)

if(require.main === module) [
    app.listen(process.env.PORT || port, () => console.log(`App is listening on port:${port}`))
]

module.exports = {app, mongoose}
